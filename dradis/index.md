---
Title: dradis
Homepage: https://dradis.com/ce/
Repository: https://gitlab.com/kalilinux/packages/dradis
Architectures: amd64
Version: 4.11.0-0kali1
Metapackages: kali-linux-everything kali-linux-large kali-tools-reporting 
Icon: images/dradis-logo.svg
PackagesInfo: |
 ### dradis
 
  Dradis is a tool to help you simplify reporting and collaboration.
   
    - Spend more time testing and less time reporting
    - Ensure consistent quality across your assessments
    - Combine the output of your favorite scanners
   
   Dradis is an open-source project released in 2007 that has been refined for
   over a decade by security professionals around the world.
 
 **Installed size:** `417.11 MB`  
 **How to install:** `sudo apt install dradis`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * bundler
 * git
 * libc6 
 * libgcc-s1 
 * libpq5 
 * libruby3.1 
 * libssl3 
 * libstdc++6 
 * lsof
 * pwgen
 * ruby 
 * zlib1g 
 {{< /spoiler >}}
 
 ##### dradis
 
 
 ```
 root@kali:~# dradis -h
 [i] Something is already using port: 3000/tcp
 COMMAND    PID   USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
 ruby3.1 215856 dradis   20u  IPv4 368387      0t0  TCP localhost:3000 (LISTEN)
 ruby3.1 215856 dradis   21u  IPv6 368388      0t0  TCP localhost:3000 (LISTEN)
 
 UID          PID    PPID  C STIME TTY      STAT   TIME CMD
 dradis    215856       1 38 05:05 ?        Ssl    0:01 puma 6.4.2 (tcp://localhost:3000
 
 [*] Please wait for the Dradis service to start.
 [*]
 [*] You might need to refresh your browser once it opens.
 [*]
 [*]  Web UI: http://127.0.0.1:3000
 
 ```
 
 - - -
 
 ##### dradis-stop
 
 
 ```
 root@kali:~# dradis-stop -h
 * dradis.service - Dradis web application
      Loaded: loaded (/usr/lib/systemd/system/dradis.service; disabled; preset: disabled)
      Active: inactive (dead)
 
 Feb 26 05:05:43 kali bundle[215856]: *          PID: 215856
 Feb 26 05:05:43 kali bundle[215856]: * Listening on http://127.0.0.1:3000
 Feb 26 05:05:43 kali bundle[215856]: * Listening on http://[::1]:3000
 Feb 26 05:05:43 kali bundle[215856]: Use Ctrl-C to stop
 Feb 26 05:05:55 kali bundle[215856]: - Gracefully stopping, waiting for requests to finish
 Feb 26 05:05:55 kali systemd[1]: Stopping dradis.service - Dradis web application...
 Feb 26 05:05:55 kali bundle[215856]: Exiting
 Feb 26 05:05:56 kali systemd[1]: dradis.service: Deactivated successfully.
 Feb 26 05:05:56 kali systemd[1]: Stopped dradis.service - Dradis web application.
 Feb 26 05:05:56 kali systemd[1]: dradis.service: Consumed 2.006s CPU time.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}


```console
service dradis start
```

## Screenshots

![Dradis login screen](images/dradis-01.png)
![Dradis dashboard](images/dradis-02.png)
![Dradis issue list](images/dradis-03.png)
![Dradis methodologies](images/dradis-04.png)
